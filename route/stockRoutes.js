const express = require('express');
const router = express.Router();

//
const { authentication } = require('../middleware/auth');

// Cotroller path
const {
  addStock,
  deleteStock,
  getStocks,
} = require('../controller/stockController');

router.route('/addStock').post(addStock);
router.route('/deleteStock/:id').delete(deleteStock);
router.route('/getStocks').get(getStocks);

module.exports = router;
