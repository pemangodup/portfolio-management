const express = require('express');
const router = express.Router();

const {
  createuser,
  deleteUser,
  login,
} = require('../controller/userController');

router.route('/createUser').post(createuser);
router.route('/deleteUser/:id').delete(deleteUser);
router.route('/login').post(login);

module.exports = router;
