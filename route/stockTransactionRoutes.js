const express = require('express');

const router = express.Router();

const { authentication } = require('../middleware/auth');

// Requiring controller path for routing
const {
  addTransaction,
  listTransaction,
  dashboardView,
  individualTransaction,
} = require('../controller/stockTransactionController');

router.route('/addTransaction').post(authentication, addTransaction);
router.route('/listTransactions').get(authentication, listTransaction);
router
  .route('/individualTransactions')
  .get(authentication, individualTransaction);
router.route('/dashboardView').get(authentication, dashboardView);

module.exports = router;
