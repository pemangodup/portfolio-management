const jwt = require('jsonwebtoken');
const User = require('../model/User');

module.exports = {
  authentication: async (req, res, next) => {
    let token;
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith('Bearer')
    ) {
      token = req.headers.authorization.split(' ')[1];
    }
    try {
      const verified = jwt.verify(token, process.env.SECRET_KEY);
      req.user = await User.findById(verified.id);
      next();
    } catch (error) {
      next(Error(error));
    }
  },
};
