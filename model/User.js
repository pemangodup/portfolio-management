const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const UserSchema = new mongoose.Schema({
  userName: {
    type: String,
    required: true,
    trim: true,
    maxlength: [35, 'Length exceeded 35 char'],
  },
  email: {
    type: String,
    unique: true,
    required: true,
    trim: true,
    match: [
      /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/,
      'Please fill a valid email id.',
    ],
  },
  password: {
    type: String,
    minlength: [6, 'Password length should be more than 6 character'],
    select: false,
  },
});

//
UserSchema.pre('save', async function () {
  const salt = await bcrypt.genSalt(10);
  this.password = await bcrypt.hash(this.password, salt);
});

UserSchema.methods.matchPassword = async function (password) {
  return await bcrypt.compare(password, this.password);
};

UserSchema.methods.getSignedJwtToken = function () {
  return jwt.sign(
    {
      id: this._id,
    },
    process.env.SECRET_KEY,
    {
      expiresIn: process.env.JWT_EXPIRES,
    }
  );
};

module.exports = mongoose.model('User', UserSchema);
