const mongoose = require('mongoose');
const StockTransactionSchema = new mongoose.Schema({
  stockName: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Stock',
    required: true,
  },
  stockQuantity: {
    type: Number,
    requiired: true,
  },
  transactionStatus: {
    type: String,
    enum: ['Buy', 'Sell'],
    required: true,
  },
  pricePerUnit: {
    type: Number,
    required: true,
    trim: true,
  },
  transactionDate: { type: Date, required: true },
});

module.exports = mongoose.model('StockTransaction', StockTransactionSchema);
