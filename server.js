const express = require('express');
const app = express();
const dotenv = require('dotenv');
const cors = require('cors');
require('colors');
const error = require('./middleware/errorResponse');
const connectDB = require('./config/db');

// Configuring dotenv files
dotenv.config({ path: 'config/config.env' });

// JSON body parser
app.use(express.json());

// cors
// app.use(cors());
var allowedOrigins = ['http://localhost:3000'];
app.use(
  cors({
    origin: function (origin, callback) {
      // allow requests with no origin
      // (like mobile apps or curl requests)
      if (!origin) return callback(null, true);
      if (allowedOrigins.indexOf(origin) === -1) {
        var msg =
          'The CORS policy for this site does not ' +
          'allow access from the specified Origin.';
        return callback(new Error(msg), false);
      }
      return callback(null, true);
    },
  })
);
// Route connection
const userRoute = require('./route/userRoutes');
const stockRoute = require('./route/stockRoutes');
const stockTransactionRoute = require('./route/stockTransactionRoutes');

// Route path middleware
app.use('/api/v1/user', userRoute);
app.use('/api/v1/stock', stockRoute);
app.use('/api/v1/stockTransaction', stockTransactionRoute);

app.use(error);

connectDB()
  .then(() => {
    app.listen(process.env.PORT, () => {
      console.log(`Listening on port number ${process.env.PORT}`.rainbow);
    });
  })
  .catch((err) => {
    console.log(err);
  });
