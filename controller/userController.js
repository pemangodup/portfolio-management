const User = require('../model/User');
module.exports = {
  // @description     Create single user
  // @route           POST /api/v1/user/createUser
  // @access          Private
  createuser: async (req, res, next) => {
    try {
      const user = await User.create(req.body);
      res.status(200).json({
        success: true,
        data: user,
      });
    } catch (error) {
      console.log('hehe');
      console.log(error);
    }
  },

  // @description     Login User
  // @route           GET api/v1/user/login
  // @access          Private
  login: async (req, res, next) => {
    const email = req.body.email;
    if (!email) {
      console.log('Email field is empty');
    }
    const user = await User.findOne({ email }).select('+password');
    const isMatch = await user.matchPassword(req.body.password.toString());
    if (!isMatch) {
      console.log('Does not match');
      return;
    }
    const token = await user.getSignedJwtToken();
    console.log(token);
    res.status(200).json({
      success: true,
      token,
    });
  },

  // @description     Delete user
  // @route           DELETE api/v1/user/deleteUser
  // @access          Private
  deleteUser: async (req, res, next) => {
    const id = req.params.id;
    const user = await User.findById(id);
    if (!user) {
      res.status(404).json({
        success: true,
        message: 'No user found',
      });
      return;
    }
    try {
      await User.findByIdAndDelete(id);
      res.status(200).json({
        success: true,
        data: {},
      });
    } catch (error) {
      console.log(error);
    }
  },
};
