const StockTransaction = require('../model/StockTransaction');

module.exports = {
  // @description     Add Stock Transaction
  // @route            POST /api/v1/addTransaction
  // @access          Private
  addTransaction: async (req, res, next) => {
    try {
      const transaction = await StockTransaction.create(req.body);
      res.status(200).json({
        success: true,
        data: transaction,
      });
    } catch (error) {
      errorResponse(res, 400, error);
    }
  },

  // @description     List All Stock Transaction
  // @route            GET /api/v1/listTransactions
  // @access          Private
  listTransaction: async (req, res, next) => {
    try {
      const transactions = await StockTransaction.find().populate(
        'stockName',
        'stockName'
      );
      res.status(200).json({
        success: true,
        data: transactions,
      });
    } catch (error) {
      errorResponse(res, 400, error);
    }
  },

  // @description     Dashboard View
  // @route           GET /api/v1/individualTransactions
  // @access          Private
  individualTransaction: async (req, res, next) => {
    let finalTotal = [];
    let script;
    let totalUnits = 0;
    let soldUnits = 0;
    let soldTotalAmount = 0;
    let boughtTotalAmount = 0;
    let totalInvestment = 0;
    let totalSoldTransactionTimes = 0;
    let totalBoughtTransactionTimes = 0;
    let scripts = [];

    const stocks = await StockTransaction.find().populate('stockName');

    // storing all the script in the array
    stocks.forEach((stock) => {
      scripts.push(stock.stockName.stockName);
    });

    // Just getting only the unique value
    const unique = scripts.filter((value, index, self) => {
      return self.indexOf(value) === index;
    });
    unique.forEach((scriptValue) => {
      stocks.forEach((stock) => {
        for (let i = 0; i > stocks.length; i++) {}
        if (stock.stockName.stockName === scriptValue) {
          script = stock.stockName.stockName;
          if (stock.transactionStatus == 'Sell') {
            soldUnits += stock.stockQuantity;
            soldTotalAmount += stock.pricePerUnit;
            totalSoldTransactionTimes += 1;
          } else {
            totalInvestment += stock.stockQuantity * stock.pricePerUnit;
            totalUnits += stock.stockQuantity;
            totalBoughtTransactionTimes += 1;
            boughtTotalAmount += stock.pricePerUnit;
          }
        }
      });

      const soldAmount = soldTotalAmount / totalSoldTransactionTimes;
      const boughtAmount = boughtTotalAmount / totalBoughtTransactionTimes;
      const profit = soldAmount * soldUnits - boughtAmount * soldUnits;
      finalTotal.push({
        script,
        totalUnits,
        soldUnits,
        soldAmount,
        boughtAmount,
        totalInvestment,
        profit,
      });
      script;
      totalUnits = 0;
      soldUnits = 0;
      soldTotalAmount = 0;
      boughtTotalAmount = 0;
      totalInvestment = 0;
      totalSoldTransactionTimes = 0;
      totalBoughtTransactionTimes = 0;
    });

    res.status(200).json({
      success: true,
      data: finalTotal,
    });
  },

  // @description     Dashboard View
  // @route           GET /api/v1/dashboard
  // @access          Private
  dashboardView: async (req, res, next) => {
    let totalUnits = 0;
    let soldUnits = 0;
    let soldTotalAmount = 0;
    let boughtTotalAmount = 0;
    let totalInvestment = 0;
    let totalSoldTransactionTimes = 0;
    let totalBoughtTransactionTimes = 0;

    const stocks = await StockTransaction.find();
    stocks.forEach((stock) => {
      if (stock.transactionStatus == 'Sell') {
        soldUnits += stock.stockQuantity;
        soldTotalAmount += stock.pricePerUnit;
        totalSoldTransactionTimes += 1;
      } else {
        totalInvestment += stock.stockQuantity * stock.pricePerUnit;
        totalUnits += stock.stockQuantity;
        totalBoughtTransactionTimes += 1;
        boughtTotalAmount += stock.pricePerUnit;
      }
    });

    const soldAmount = soldTotalAmount / totalSoldTransactionTimes;
    const boughtAmount = boughtTotalAmount / totalBoughtTransactionTimes;
    const profit = soldAmount * soldUnits - boughtAmount * soldUnits;

    res.status(200).json({
      success: true,
      totalUnits,
      soldUnits,
      soldAmount,
      boughtAmount,
      totalInvestment,
      profit,
      data: stocks,
    });
  },
};

// Error response function
function errorResponse(res, statusCode, error) {
  res.status(400).json({
    success: false,
    message: error.message,
  });
}
