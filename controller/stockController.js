const Stock = require('../model/Stock');

module.exports = {
  // @description     Add Stock
  // @route           POST /api/v1/addStock
  // @access          Private
  addStock: async (req, res, next) => {
    try {
      const stock = await Stock.create(req.body);
      res.status(200).json({
        success: true,
        data: stock,
      });
    } catch (error) {
      next(Error(error));
    }
  },

  // @description     List Stock
  // @route           GET /api/v1/addStock
  // @access          Private
  getStocks: async (req, res, next) => {
    const stocks = await Stock.find();
    res.status(200).json({
      success: true,
      data: stocks,
    });
  },

  // @description     Delete Stock
  // @route           DELETE /api/v1/deleteStock
  // @access          Private
  deleteStock: async (req, res, next) => {
    console.log('Reached here');
    console.log(req.params.id);
  },
};
